/**
 * Created by myakubov on 02/04/2014.
 */
package com.ebs.apollo.dal.communication {

import mx.messaging.ChannelSet;

import mx.messaging.Consumer;
import mx.messaging.events.ChannelEvent;
import mx.messaging.events.ChannelFaultEvent;
import mx.messaging.events.MessageAckEvent;
import mx.messaging.events.MessageEvent;
import mx.messaging.events.MessageFaultEvent;

public class ManagedConsumer extends Consumer {

    public function ManagedConsumer(_channelSet:ChannelSet, _destination:String, _subtopic:String) {
        channelSet = _channelSet;
        destination = _destination;
        subtopic = _subtopic;
    }

    override public function subscribe(clientId:String = null):void {
        super.subscribe(clientId);

        addEventListener(MessageEvent.MESSAGE, messageHandler, false, 0, false);
        addEventListener(MessageAckEvent.ACKNOWLEDGE, messageAckHandler, false, 0, false);
        addEventListener(MessageFaultEvent.FAULT, messageFaultHandler, false, 0, false);
    }


    override public function unsubscribe(preserveDurable:Boolean = false):void {
        super.unsubscribe(preserveDurable);

        removeEventListener(MessageEvent.MESSAGE, messageHandler);
        removeEventListener(MessageAckEvent.ACKNOWLEDGE, messageAckHandler);
        removeEventListener(MessageFaultEvent.FAULT, messageFaultHandler);
    }

    //=======================================================
    //                 EVENT HANDLERS
    //=======================================================

    protected function messageHandler(event:MessageEvent) : void {
       //trace("ManagedConsumer: **** messageHandler ****");
    }

    protected function messageAckHandler(event:MessageAckEvent) : void {
        //trace("Consumer: **** messageAckHandler ");
    }

    protected function messageFaultHandler(event:MessageFaultEvent) : void {
        trace("Consumer: **** messageFaultHandler ");
    }

    //=======================================================
    //                  OVERRIDES
    //=======================================================

    override public function channelConnectHandler(event:ChannelEvent):void{
        trace("Consumer: **** channelConnectHandler ");// + event.toString());
        super.channelConnectHandler(event);
    }

    override public function channelDisconnectHandler(event:ChannelEvent) : void {
        trace("Consumer: **** channelDisconnectHandler ");// + event.toString());
        super.channelDisconnectHandler(event);
    }

    override public function channelFaultHandler(event:ChannelFaultEvent) : void {
        trace("Consumer: **** channelFaultHandler ");// + event.toString());
        super.channelFaultHandler(event);
    }
}
}
