/**
 * Created by myakubov on 29/04/2014.
 */
package com.ebs.apollo.dal.communication {
import com.ebs.apollo.Constants.RemoteServices;
import com.ebs.apollo.domain.model.DM;

import mx.messaging.ChannelSet;
import mx.rpc.AbstractOperation;
import mx.rpc.AsyncToken;
import mx.rpc.Responder;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.rpc.http.Operation;
import mx.rpc.remoting.RemoteObject;

public class ManagedRemoteObject {

    public static const REQUEST_ID_PROP:String = "requestId";

    private var requestCounter:uint = 0;
    private var remoteObject:RemoteObject;

    public function ManagedRemoteObject(_channelSet:ChannelSet, _destination:String) {
        remoteObject = new RemoteObject();
        remoteObject.channelSet = _channelSet;
        remoteObject.destination = _destination;
        remoteObject.addEventListener(ResultEvent.RESULT, resultHandler);
        remoteObject.addEventListener(FaultEvent.FAULT, faultHandler);
    }

    public function remoteCall(requestType:RemoteServices, args:Array, responseCallback:Function=null, errorCallback:Function=null, addDefaultProps:Boolean=true):void{
        requestCounter++;
        if(addDefaultProps){
            args.push(requestCounter);
            args.push(DM.instance.gaUserID);
        }

        var operation:AbstractOperation = remoteObject.getOperation(requestType.name);
        operation.arguments = args;
        var token:AsyncToken = operation.send();
        token[REQUEST_ID_PROP] = requestCounter;
        token.addResponder(new Responder(responseCallback, errorCallback));
    }

    //=======================================================
    //                 EVENT HANDLERS
    //=======================================================

    protected function resultHandler(event:ResultEvent) : void {
        //trace("MangedRemoteObject: **** resultHandler ****" + event.toString());
    }

    protected function faultHandler(event:FaultEvent) : void {
        trace("MangedRemoteObject: **** faultHandler ****");// + event.toString());
    }
}
}
