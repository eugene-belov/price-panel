/**
 * Created by myakubov on 02/04/2014.
 */
package com.ebs.apollo.dal.communication {
import com.ebs.apollo.Constants.RemoteServices;
import com.ebs.apollo.domain.model.DM;


import mx.messaging.Channel;

import mx.messaging.ChannelSet;
import mx.messaging.channels.AMFChannel;
import mx.messaging.channels.SecureAMFChannel;
import mx.messaging.channels.SecureStreamingAMFChannel;
import mx.messaging.channels.SecureStreamingHTTPChannel;
import mx.messaging.channels.StreamingAMFChannel;
import mx.messaging.events.ChannelEvent;
import mx.messaging.events.ChannelFaultEvent;
import mx.utils.ObjectUtil;

public class Communicator{

    //general prefix for all topics
    private const TOPIC_PREFIX:String = "cl.cl";
    private const MESSAGING_DESTINATION:String = "PushDestination";
    private const REMOTING_DESTINATION:String = "flexR3Service";
    private const STRIMING_CHANNEL_ID:String = "push-channel-amf";
    private const POLLING_CHANNEL_ID:String = "req-res-channel-amf";

    private var _channelSetStreaming:ChannelSet;
    private var _channelSetPolling:ChannelSet;
    private var managedConsumers:Vector.<ManagedConsumer>;
    private var managedRemoteObject:ManagedRemoteObject;

    private var host:String = "http://10.20.30.249:8083";
    private var hostSecure:String = "https://10.20.30.249:8443";
    private var contextStreaming:String = "/app/messagebroker/streamingamf";
    private var contextPolling:String = "/app/messagebroker/amfasync";

    public function Communicator() {
    }

    public function init():void {
        //Stream channels
        _channelSetStreaming = new ChannelSet();
        var channel:Channel;
        channel = getSecureStreamingChanel();
        addChannelEventListeners(channel);
        _channelSetStreaming.addChannel(channel);
        channel = getSecureStreamingHTTPChannel();
        addChannelEventListeners(channel);
        _channelSetStreaming.addChannel(channel);

        //Polling channels
        var pollingChannel:AMFChannel = getSecurePollingChanel();
        addChannelEventListeners(pollingChannel);
        pollingChannel.pollingEnabled = true;
        pollingChannel.pollingInterval = 3000;

        _channelSetPolling = new ChannelSet();
        _channelSetPolling.addChannel(pollingChannel);

        managedConsumers = new Vector.<ManagedConsumer>();
        managedRemoteObject = new ManagedRemoteObject(_channelSetPolling, REMOTING_DESTINATION);
    }

    //private function setupSecureChannelSet():
    public function getConsumerByTopic(subtopic:String):ManagedConsumer{
        var consumer:ManagedConsumer;
        var topic:String = TOPIC_PREFIX + DM.instance.gaUserID + "." + subtopic;

        for each (var item in managedConsumers) {
            if(item.subtopic == topic){
                consumer = item;
                break;
            }
        }

        if(!consumer){
            consumer = new ManagedConsumer(_channelSetStreaming, MESSAGING_DESTINATION, topic);
            managedConsumers.push(consumer);
        }

        return consumer;
    }

    public function removeConsumerByTopic(subtopic:String):void{
        var consumer:ManagedConsumer;
        var topic:String = TOPIC_PREFIX + "." + subtopic;
        for each (consumer in managedConsumers) {
            if(consumer.subtopic == topic){
                break;
            }
        }

        if(!consumer){
            if (consumer.subscribed) {
                consumer.unsubscribe();
            }
            consumer.disconnect();
            managedConsumers.splice(managedConsumers.indexOf(consumer), 1);
        }
    }

    public function remoteCall(requestType:RemoteServices, args:Array, responseCallback:Function=null, errorCallback:Function=null, addDefaultProps:Boolean=true):void{
        managedRemoteObject.remoteCall(requestType, args, responseCallback, errorCallback, addDefaultProps);
    }

    private function getPollingChanel():AMFChannel{
        return new AMFChannel(POLLING_CHANNEL_ID, host + contextPolling);
    }

    private function getSecurePollingChanel():SecureAMFChannel{
        return new SecureAMFChannel(POLLING_CHANNEL_ID, hostSecure + contextPolling);
    }

    private function getStreamingChanel():StreamingAMFChannel{
        return new StreamingAMFChannel(STRIMING_CHANNEL_ID, host + contextStreaming);
    }

    private function getSecureStreamingChanel():SecureStreamingAMFChannel{
        return new SecureStreamingAMFChannel(STRIMING_CHANNEL_ID, hostSecure + contextStreaming);
    }

    private function getSecureStreamingHTTPChannel():SecureStreamingHTTPChannel{
        return new SecureStreamingHTTPChannel(STRIMING_CHANNEL_ID, hostSecure + contextStreaming);
    }

    private function addChannelEventListeners(channel:Channel):void{
        channel.addEventListener(ChannelFaultEvent.FAULT, channelFaultHandler);
        channel.addEventListener(ChannelEvent.CONNECT, channelConnectHandler);
        channel.addEventListener(ChannelEvent.DISCONNECT, channelDisconnectHandler);
    }

    private function channelFaultHandler(event:ChannelFaultEvent):void {
        trace("****ERROR***** Communicator.channelFaultHandler: ");// + ObjectUtil.toString(event));
    }

    private function channelConnectHandler(event:ChannelEvent):void {
        //trace("Communicator.channelConnectHandler: " + ObjectUtil.toString(event));
    }

    private function channelDisconnectHandler(event:ChannelEvent):void {
        trace("****ERROR***** Communicator.channelDisconnectHandler: ");// + ObjectUtil.toString(event));
    }
}
}
