/**
 * Created with IntelliJ IDEA.
 * User: myakubov
 * Date: 27/11/13
 * Time: 10:44
 * To change this template use File | Settings | File Templates.
 */
package com.ebs.apollo.dal.Objects {

public class TopicListener {

    private var _id:String;
    private var _listeners:Vector.<Function>;

    public function TopicListener(listenerId:String) {
        _id = listenerId;
        _listeners= new Vector.<Function>;
    }

    public function addListener(handler:Function):void{
        if(_listeners.indexOf(handler) == -1){
            _listeners.push(handler);
        }
    }

    public function removeListener(handler:Function):void{
        var index:int = _listeners.indexOf(handler);
        if(index == -1){
            _listeners.splice(index, 1);
        }
    }

    public function clearListener():void{
        _listeners = null;
        _listeners = new Vector.<Function>;
    }

    public function notifyListener(message:*):void{
        for each (var curFunc:Function in _listeners){
            curFunc(message);
        }
    }

    public function get id():String {
        return _id;
    }
}
}
