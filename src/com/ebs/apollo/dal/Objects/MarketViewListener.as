/**
 * Created with IntelliJ IDEA.
 * User: myakubov
 * Date: 27/11/13
 * Time: 10:44
 * To change this template use File | Settings | File Templates.
 */
package com.ebs.apollo.dal.Objects {
import com.ebs.apollo.Constants.MVType;


public class MarketViewListener extends TopicListener {

    private var _currencyPair:String;
    private var _mvType:MVType;

    public function MarketViewListener(ccyPair:String, type:MVType) {
        _mvType = type;
        _currencyPair = ccyPair;
        super(_currencyPair + _mvType);
    }

    public function get mvType():MVType {
        return _mvType;
    }

    public function get currencyPair():String {
        return _currencyPair;
    }
}
}
