package com.ebs.apollo.dal.Objects {
public class CurrencyPairSubscription extends TopicListener {

    private var _currencyPair:String;

    public function CurrencyPairSubscription(ccyPair:String) {
        _currencyPair = ccyPair;
        super(_currencyPair);
    }

    public function get currencyPair():String {
        return _currencyPair;
    }
}
}
