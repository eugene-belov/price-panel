/**
 * Created with IntelliJ IDEA.
 * User: myakubov
 * Date: 27/11/13
 * Time: 10:44
 * To change this template use File | Settings | File Templates.
 */
package com.ebs.apollo.dal.Objects {

public class OrderSubscription extends TopicListener {

    private var _clientOrderReference:String;

    public function OrderSubscription(clientOrderRefId:String) {
        _clientOrderReference = clientOrderRefId;
        super(_clientOrderReference);
    }

    public function get clientOrderReference():String {
        return _clientOrderReference;
    }
}
}
