/**
 * Created by myakubov on 01/05/2014.
 */
package com.ebs.apollo.dal.services {


import com.ebs.apollo.Constants.MVType;
import com.ebs.apollo.Constants.RemoteServices;
import com.ebs.apollo.dal.Objects.TopicListener;
import com.ebs.apollo.dal.Objects.MarketViewListener;
import com.ebs.apollo.domain.objects.R3MvSubscriptionResult;
import com.ebs.apollo.domain.objects.icap.spot.common.domain.BatchMV;
import com.ebs.apollo.utils.types.apolloLogId;


import mx.collections.ArrayCollection;
import mx.messaging.events.MessageEvent;


import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

public class MarketViewService extends BaseService {

    public static const LOG_ID:String = apolloLogId(MarketViewService);
    public static const TOPIC:String = "MV";

    public function MarketViewService(traderId:String, floorId:String, region:String, userId:String) {
        super(traderId, floorId, region, userId, LOG_ID, TOPIC);
    }

    /**
     * Listener Function would receive  MsgBase
     * @param currencyPair
     * @param mvType
     * @param listenerCallBack
     */
    public function addMVListener(currencyPair:String, mvType:MVType, listenerCallBack:Function):void {
        var mvListener:MarketViewListener = new MarketViewListener(currencyPair, mvType);
        addTopicListener(getSubjectId(mvListener), mvListener, listenerCallBack);
    }

    public function removeMVListener(currencyPair:String, mvType:MVType, listenerCallBack:Function):void {
        var mvListener:MarketViewListener = new MarketViewListener(currencyPair, mvType);
        removeTopicListener(getSubjectId(mvListener), mvListener, listenerCallBack);
    }

    private function getSubjectId(mvListener:MarketViewListener):String{
        return mvListener.currencyPair +"." + mvListener.mvType.name;
    }

    /**
     * For market views we need to send message to server on which instrument
     * we wish to assign.
     * @param subjectId
     * @param subscriber
     */
    override protected function subscribe(subjectId:String, topicListener:TopicListener):void {
        var mvSub:MarketViewListener = topicListener as MarketViewListener;
        remoteCall(RemoteServices.ADD_MV_LISTENER, [mvSub.mvType.type, mvSub.currencyPair], subscribeConfirm, subscribeFault);
    }

    override protected function messageHandler(event:MessageEvent):void {
        trace("got batch MV");

        var batchMv:BatchMV = event.message.body as BatchMV
        notifyByType("curPair", MVType.WS_PRICES, batchMv.wsPrices);
        notifyByType("curPair", MVType.RATES, batchMv.rates);
        notifyByType("pair", MVType.MARKET_DATA, batchMv.marketData);
        notifyByType("curPair", MVType.INDICATIVE_RATES, batchMv.mvIndicativeRates);
        notifyByType("currencyPairName", MVType.EBS_DEALS, batchMv.ebsDeals);
    }

    private function subscribeConfirm(result:ResultEvent):void{
        trace("MarketViewListener.subscribeConfirm: ");// + ObjectUtil.toString(result));
        var a:R3MvSubscriptionResult = result.result as R3MvSubscriptionResult;
    }

    private function subscribeFault(event:FaultEvent):void{
        trace("****ERROR***** MarketViewListener.handleMVListenerFault: ");// + ObjectUtil.toString(event));
    }

    private function notifyByType(pairPropName:String, mvType:MVType, mvCollection:ArrayCollection):void {
        var mvListener:MarketViewListener;
        for each (var currMV:* in mvCollection) {
            mvListener = new MarketViewListener(currMV[pairPropName], mvType);
            notifyListeners(getSubjectId(mvListener), mvListener, currMV);
        }
    }

}
}
