/**
 * Created by myakubov on 01/05/2014.
 */
package com.ebs.apollo.dal.services {
import com.ebs.apollo.Constants.RemoteServices;
import com.ebs.apollo.domain.objects.R3LoginResult;
import com.ebs.apollo.domain.objects.R3UserOperationError;
import com.ebs.apollo.utils.types.apolloLogId;

import mx.rpc.Fault;


import mx.rpc.events.FaultEvent;

import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

public class LoginService extends BaseService{

    public static const LOG_ID:String = apolloLogId(LoginService);
    //public static const TOPIC_NAME:String = className(ApolloLoginResult);

    private var _resultCallBack:Function;
    private var _errorCallBack:Function;

    /**
     * @param resultCallBack(R3LoginResult)
     * @param errorCallBack(R3UserOperationError)
     */
    public function LoginService(traderId:String, floorId:String, region:String, resultCallBack:Function, errorCallBack:Function) {
        super(traderId, floorId, region, LOG_ID, null);
        _resultCallBack = resultCallBack;
        _errorCallBack = errorCallBack;
    }

    public function login():void {

        //Note: when THIS login completed on server and result arrives to client,
        //client must send additional command to server
        //to notify the server about UI readiness to accept HB.

        //TODO<Michael>: secretToken should be invoked on each re\login? --> PETER
        remoteCall(RemoteServices.LOGIN, [_region, _traderId, _floorId, "secretToken"], loginResultHandler, loginErrorHandler, false);
    }

    private function loginResultHandler(event:ResultEvent):void{
        if(_resultCallBack) {
            var loginResult:R3LoginResult = event.message.body as R3LoginResult;
            var loginError:R3UserOperationError = loginResult ? loginResult.error : null;
            if(loginError && _errorCallBack){
                _errorCallBack(loginError);
            }else {
                _resultCallBack(loginResult);
            }
        }
    }

    private function loginErrorHandler(faultEvent:FaultEvent=null):void{
        //TODO: handle communication error... how?
        trace("******ERROR******** LoginService.loginErrorHandler: ");// + ObjectUtil.toString(faultEvent));
    }
}
}
