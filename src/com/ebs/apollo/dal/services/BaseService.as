/**
 * Created by myakubov on 30/04/2014.
 */
package com.ebs.apollo.dal.services {
import com.ebs.apollo.Constants.RemoteServices;
import com.ebs.apollo.dal.Objects.TopicListener;
import com.ebs.apollo.dal.communication.ManagedConsumer;
import com.ebs.apollo.dal.communication.ManagedRemoteObject;
import com.ebs.apollo.domain.model.DM;

import flash.utils.Dictionary;

import fusion.core.logging.logger.ILogger;
import fusion.core.logging.loggerManager.LoggerManager;

import mx.messaging.events.ChannelEvent;

import mx.messaging.events.ChannelFaultEvent;

import mx.messaging.events.MessageEvent;
import mx.utils.ObjectUtil;

public class BaseService {

    /** Loggers */
    private var _log:ILogger;
    private var _clientLog:ILogger;

    protected var _topic:String=null;
    protected var _traderId:String;
    protected var _floorId:String;
    protected var _region:String;
    protected var _userId:String;

    protected var consumer:ManagedConsumer;
    private var _topics:Dictionary = new Dictionary();

    public function BaseService(traderId:String, floorId:String, region:String, userId:String, logId:String, topic:String=null) {
        _traderId = traderId;
        _floorId = floorId;
        _region = region;
        _userId = userId;
        _topic = topic;

        _log = LoggerManager.getLogger(logId);
        _clientLog = LoggerManager.getSpecificLogger("apolloService", false, logId);
    }

    //===================================================//
    //              PUBLIC METHODS                       //
    //===================================================//

    public function connectService(topic:String=null):void{
        if(topic != null) {
            _topic = topic;
        }
        //clear previous consumer..
        if(consumer){
            consumer.unsubscribe();
            consumer.disconnect();
            consumer = null;
            removeConsumerListeners();
        }
        //Subscribe new consumer
        if(_topic) {
            consumer = DM.instance.getConsumerByTopic(_topic);
            consumer.subscribe();
            addConsumerListeners();
        }
    }

    //===================================================//
    //              PROTECTED METHODS                    //
    //===================================================//

    protected function remoteCall(requestType:RemoteServices, args:Array, responseCallback:Function=null, errorCallback:Function=null, addDefaultProps:Boolean=true):void{
        DM.instance.remoteCall(requestType, args, responseCallback, errorCallback, addDefaultProps);
    }

    protected function get isConsumerReady():Boolean{
        return (consumer && consumer.connected && consumer.subscribed);
    }

    /**
     * Listener Function would receive  MsgBase
     * @param currencyPair
     * @param mvType
     * @param listenerCallBack
     */
    protected function addTopicListener( subjectId:String,
                                         topicListener:TopicListener,
                                         listenerCallBack:Function):void {
        if(!topicListener){
            return;
        }
        var topicListeners:Vector.<TopicListener> = addTopicSubject(subjectId);
        var existingListener:TopicListener = findListener(subjectId, topicListener);
        //If not found its new subscription, send subscription message.
        if(existingListener){
            existingListener.addListener(listenerCallBack);
        }else{
            topicListener.addListener(listenerCallBack);
            topicListeners.push(topicListener);
            subscribe(subjectId, topicListener);
        }
    }

    protected function addTopicSubject(subjectId:String):Vector.<TopicListener>{
        if(!_topics[subjectId]){
            _topics[subjectId] = new Vector.<TopicListener>();
        }
        return _topics[subjectId];
    }

    protected function removeTopicSubject(subjectId:String):void{
        if(!_topics[subjectId]){
            _topics[subjectId] = null;
        }
    }

    protected function removeTopicListener( subjectId:String,
                                            topicListener:TopicListener,
                                            listenerCallBack:Function):void{
        var listener:TopicListener = findListener(subjectId, topicListener);
        if(listener){
            listener.removeListener(listenerCallBack);
        }
    }

    protected function addConsumerListeners():void{
        consumer.addEventListener(MessageEvent.MESSAGE, messageHandler, false, 0, false);
        consumer.addEventListener(MessageEvent.RESULT, resultHandler, false, 0, false);

        consumer.addEventListener(ChannelFaultEvent.FAULT, channelFaultHandler, false, 0, false);
        consumer.addEventListener(ChannelEvent.CONNECT, channelConnectHandler, false, 0, false);
        consumer.addEventListener(ChannelEvent.DISCONNECT, channelDisConnectHandler, false, 0, false);
    }

    protected function removeConsumerListeners() : void{
        consumer.removeEventListener(MessageEvent.MESSAGE, messageHandler);
        consumer.removeEventListener(MessageEvent.RESULT, resultHandler);

        consumer.removeEventListener(ChannelFaultEvent.FAULT, channelFaultHandler);
        consumer.removeEventListener(ChannelEvent.CONNECT, channelConnectHandler);
        consumer.removeEventListener(ChannelEvent.DISCONNECT, channelDisConnectHandler);
    }

    /**
     *
     * @param subjectId: subscription id
     * @param callBack: id compare\verification function call back
     * @return
     */
    protected function findListener(subjectId:String, listener:TopicListener):TopicListener{
        var retVal:TopicListener = null;
        var subscribersVec:Vector.<TopicListener> = _topics[subjectId];
        for each (var currItem:TopicListener in subscribersVec) {
            if(listenerCompare(currItem, listener)){
                retVal = currItem;
                break;
            }
        }
        return retVal;
    }

    protected function notifyListeners(subjectId:String, subscription:TopicListener, message:*):void{
        var listener:TopicListener = findListener(subjectId, subscription);
        if(listener){
            listener.notifyListener(message);
        }
    }

    /**
     * Override for custom subscribers compare..
     * @param subA
     * @param subB
     * @return
     */
    protected function listenerCompare(listenerA:TopicListener, listenerB:TopicListener):Boolean{
        return (listenerA.id == listenerB.id);
    }

    protected function subscribe(subjectId:String, topicListener:TopicListener):void {
        throw new Error("BusServices.subscribe: MUST OVERRIDE!!!");
    }

    //===================================================//
    //              PRIVATE METHODS                      //
    //===================================================//

    //===================================================//
    //              EVENT HANDLERS                       //
    //===================================================//

    protected function messageHandler(event:MessageEvent):void{
        //trace("BaseService.messageHandler : " + ObjectUtil.toString(event));
    }

    protected function resultHandler(event:MessageEvent):void{
        //trace("BaseService.resultHandler : " + ObjectUtil.toString(event));
    }

    protected function channelFaultHandler(event:ChannelFaultEvent):void{
        trace("****ERROR***** BaseService.channelFaultHandler : ");// + ObjectUtil.toString(event));
    }

    protected function channelConnectHandler(event:ChannelEvent):void{
        //trace("BaseService.channelConnectHandler : " + ObjectUtil.toString(event));
    }

    protected function channelDisConnectHandler(event:ChannelEvent):void{
        trace("****ERROR***** BaseService.channelDisConnectHandler : ");// + ObjectUtil.toString(event));
    }
}
}
