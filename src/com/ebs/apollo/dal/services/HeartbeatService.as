/**
 * Created by myakubov on 01/05/2014.
 */
package com.ebs.apollo.dal.services {

import com.ebs.apollo.Constants.RemoteServices;
import com.ebs.apollo.domain.model.DM;
import com.ebs.apollo.domain.objects.R3HeartbeatFromClient;
import com.ebs.apollo.domain.objects.R3HeartbeatToClient;
import com.ebs.apollo.utils.types.apolloLogId;

import flash.events.TimerEvent;

import flash.utils.Timer;

import mx.messaging.events.MessageEvent;

import mx.rpc.events.FaultEvent;

import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

public class HeartbeatService extends BaseService{

    public static const LOG_ID:String = apolloLogId(HeartbeatService);
    public static const TOPIC:String = "HB";

    private var consumerReadyTimer:Timer = new Timer(1000, 0);

    public function HeartbeatService(traderId:String, floorId:String, region:String, userId:String) {
        super(traderId, floorId, region, userId, LOG_ID, TOPIC);
    }

    public function start():void {
        consumerReadyTimer.addEventListener(TimerEvent.TIMER, consumerReadyTimerHandler);
        consumerReadyTimer.reset();
        consumerReadyTimer.start();
    }

    public function stop():void {
        consumerReadyTimer.stop();
        consumerReadyTimer.removeEventListener(TimerEvent.TIMER, consumerReadyTimerHandler);
        removeConsumerListeners();
    }

    override protected function messageHandler(event:MessageEvent):void {
        trace("HB");
        var hbToClient:R3HeartbeatToClient =  event.message.body as R3HeartbeatToClient;
        //TODO<Michael>: correct client time, taking server time from the heartbeat!

        //send HB back to server
        var heartbeatFromClient:R3HeartbeatFromClient = new R3HeartbeatFromClient();
        heartbeatFromClient.timestamp = (new Date()).time.toString();
        heartbeatFromClient.userId = hbToClient.userId;

        remoteCall(RemoteServices.SEND_HB, [heartbeatFromClient], sendHbResponseHandler, sendHbErrorHandler, false);
    }

    private function consumerReadyTimerHandler(event:TimerEvent):void {
        if(isConsumerReady){
            consumerReadyTimer.removeEventListener(TimerEvent.TIMER, consumerReadyTimerHandler);
            //this command gives server to know, that it can start sending HB
            remoteCall(RemoteServices.CONFIRM_HB_READY, [DM.instance.gaUserID], confirmHbConsumerReadyHandler, confirmHbConsumerReadyErrorHandler, false);
        }else{
            consumerReadyTimer.reset();
            consumerReadyTimer.start();
        }
    }

    private function confirmHbConsumerReadyHandler(event:ResultEvent):void{
        trace("HeartbeatService.confirmHbConsumerReadyHandler, Begin HB");
    }

    private function confirmHbConsumerReadyErrorHandler(event:FaultEvent):void{
        trace("****ERROR***** confirmHbConsumerReadyErrorHandler: ");// + ObjectUtil.toString(event));
    }

    private function sendHbResponseHandler(event:ResultEvent):void{
        //trace("HeartbeatService.sendHbResponseHandler");
    }

    private function sendHbErrorHandler(event:FaultEvent):void{
        trace("****ERROR***** sendHbErrorHandler: ");// + ObjectUtil.toString(event));
    }

}
}
