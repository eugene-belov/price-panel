package com.ebs.apollo.view.components {
import spark.components.NumericStepper;

public class NumericStepper extends spark.components.NumericStepper {

    public function NumericStepper() {
        super();
        setStyle("skinClass", NumericStepperSkin);
        setStyle("focusThickness", 0);
    }
}
}
