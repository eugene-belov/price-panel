package com.ebs.apollo.view.components.buttons {
import spark.components.Button;

public class MonochromeButton extends Button {

    [Inspectable(category="General", format="Color")]
    [Bindable] public var color:uint;
    [Inspectable(category="General", format="Color")]
    [Bindable] public var selectionColor:uint;
    [Inspectable(category="General", format="Color")]
    [Bindable] public var textColor:uint;

    public function MonochromeButton() {
        super();
        height = 17;
        setStyle("skinClass", MonochromeButtonSkin);
    }
}
}
