package com.ebs.apollo.view.components.buttons {
import spark.components.Button;

public class SolidButton extends Button {

    [Inspectable(category="General", format="Color")]
    [Bindable]
    public var backgroundColor:uint = 0x393939;

    public function SolidButton() {
        super();
        setStyle("skinClass", SolidButtonSkin);
        setStyle("cornerRadius", 0);
    }
}
}
