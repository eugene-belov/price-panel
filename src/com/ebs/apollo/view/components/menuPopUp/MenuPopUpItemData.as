/**
 * Created by myakubov on 10/04/2014.
 */
package com.ebs.apollo.view.components.menuPopUp {
public class MenuPopUpItemData {

    public var label:String;
    public var isSeparator:Boolean = false;

    public function MenuPopUpItemData(label:String=null, isSeparator:Boolean=false) {
        this.label = label;
        this.isSeparator = isSeparator;
    }
}
}
