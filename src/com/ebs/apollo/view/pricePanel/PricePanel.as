package com.ebs.apollo.view.pricePanel
{

import flash.events.Event;

import mx.events.FlexEvent;

import spark.components.SkinnableContainer;

	public class PricePanel extends SkinnableContainer
	{
        [SkinPart(required="true")]
        public var header:Header;
        [SkinPart(required="true")]
        public var subHeader:SubHeader;
        [SkinPart(required="true")]
        public var body:Body;

        public function PricePanel()
		{
			super();
            setStyle("skinClass", PricePanelBaseSkin);

            callLater(handlePriceUpdate);
		}

        private function handlePriceUpdate():void{

            //Sub header
            //==========
            subHeader.bidBigFigure = "62.";
            subHeader.bidPips = "49";
            subHeader.offerBigFigure = "62.";
            subHeader.offerPips = "51";

            //Body
            //==========
            body.bidRegular = "56.0";
            body.bidAmountView = "5";
            body.bidDealableBestAmount = "10";
            body.bidDealablePlusAmount = "0";

            body.bidRegularColor = "gray";
            body.bidAmountViewColor = "gray";
            body.bidDealableBestAmountColor = "white";
            body.bidDealablePlusAmountColor = "white";
//            body.bidAggressBgColor = "green";
//            body.bidAggressBgDownColor = "yellow";

            body.offerRegular = "56.5";
            body.offerAmountView = "10";
            body.offerDealableBestAmount = "11";
            body.offerDealablePlusAmount = "1";

            body.offerRegularColor = "gray";
            body.offerAmountViewColor = "gray";
            body.offerDealableBestAmountColor = "white";
            body.offerDealablePlusAmountColor = "white";
//            body.offerAggressBgColor = "green";
//            body.offerAggressBgDownColor = "yellow";

        }
	}
}