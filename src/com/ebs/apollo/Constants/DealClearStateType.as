package com.ebs.apollo.Constants{
import com.ebs.apollo.utils.commons.Enum;

public class DealClearStateType extends Enum {
    // Deal Clear State values
    public static const NOT_CLEARED: MVType = new MVType(0);
    public static const CLEARED_COMPLETED: MVType = new MVType(1);
    public static const CLEARED_UNVERIFIED: MVType = new MVType(2);

    private var _type:uint;

    public function DealClearStateType(typeId:uint):void{
        super(String(typeId), _);
        _type = typeId;
    }

    public function get type():uint {
        return _type;
    }

    override protected function getConstants():Array {
        return [NOT_CLEARED, CLEARED_COMPLETED, CLEARED_UNVERIFIED];
    }
}
}

