package com.ebs.apollo.Constants {
public class MarketDataKeys {
    public static const EBS_BEST_ID:int = 11;

    // Key for dealable best price.  Downcast corresponding value object to
    public static const DEALABLE_BEST_ID:int = 12;
    public static const DEALABLE_REGULAR_ID:int = 13;
    public static const LOCAL_PRICE_ID:int = 14;
    public static const WIDE_SPREAD_ID:int = 15;
    public static const STALE_ID:int = 16;

    // KEYS FOR MARKET VIEW MARKET RATE DATA:
    public static const MARKET_RATE_ID:int = 17;

    // KEY FOR MARKET VIEW DEAL DATA:
    public static const DEAL_ID:int = 18;
    public static const EMPTY_UPDATE_ID:int = 19;

    // KEY FOR DoB Amounts from broker to RTV update
    public static const DEALABLE_AMOUNTS_ID:int = 20;

    // KEY FOR DoB Amounts from RTV to W/S update
    public static const OUTSIDE_PRICE_AND_AMOUNTS_ID:int = 21;

    // KEYS FOR MARKET VIEW MARKET DATA:
    public static const CURRENT_OPEN_BID_ID:int = 22;
    public static const CURRENT_OPEN_OFFER_ID:int = 23;
    public static const CURRENT_CLOSE_BID_ID:int = 45;
    public static const CURRENT_CLOSE_OFFER_ID:int = 46;
    public static const CURRENT_ABSOLUTE_LOW_ID:int = 24;
    public static const CURRENT_ABSOLUTE_HIGH_ID:int = 25;
    public static const CURRENT_MARKET_LOW_ID:int = 26;
    public static const CURRENT_MARKET_HIGH_ID:int = 27;
    public static const PRIOR_DAY_OPEN_BID_ID:int = 28;
    public static const PRIOR_DAY_OPEN_OFFER_ID:int = 29;
    public static const PRIOR_DAY_ABSOLUTE_LOW_ID:int = 30;
    public static const PRIOR_DAY_ABSOLUTE_HIGH_ID:int = 31;
    public static const PRIOR_DAY_MARKET_LOW_ID:int = 32;
    public static const PRIOR_DAY_MARKET_HIGH_ID:int = 33;
    public static const PRIOR_DAY_CLOSE_BID_ID:int = 34;
    public static const PRIOR_DAY_CLOSE_OFFER_ID:int = 35;
    public static const MARKET_DATA_STATUS_ID:int = 36;
    public static const OTHER_LOCAL_PRICE_ID:int = 37;
    public static const OTHER_LOCAL_PRICE_AI_ID:int = 38;
    public static const OTHER_LOCAL_PRICE_WS_ID:int = 39;
    public static const NET_VOUME_FLOW_ID:int = 40;
    public static const REGIONAL_DATA_ID:int = 41;

}
}