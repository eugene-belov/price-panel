/**
 * Created with IntelliJ IDEA.
 * User: myakubov
 * Date: 25/12/13
 * Time: 14:26
 * To change this template use File | Settings | File Templates.
 */
package com.ebs.apollo.Constants {
public class OrderStatusType {

    public static const	AVAILABLE           : int = 1;
    public static const	INTERRUPTED         : int = 2;
    public static const	CANCELLED           : int = 3;
    public static const	COMPLETED           : int = 4;
    public static const DEAL_PROPOSED       : int = 5;
    public static const INTERRUPTING        : int = 6;
    public static const MISSED_QUOTE_ACTIVE : int = 7;
    public static const SKEWING             : int = 8;

}
}
