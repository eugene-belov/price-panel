package com.ebs.apollo.Constants{
import com.ebs.apollo.utils.commons.Enum;

public class VenueType extends Enum {

    public static const NONE:VenueType = new VenueType("NONE"); // Temporary
    public static const EBS:VenueType  = new VenueType("EBS");
    public static const EBS_SEF:VenueType  = new VenueType("EBS_SEF");

    public function VenueType(name:String) {
        super(name, _);
    }

    override protected function getConstants():Array {
        return [NONE, EBS, EBS_SEF];
    }
}
}

