package com.ebs.apollo.Constants{

import com.ebs.apollo.utils.commons.Enum;

public class OrderActionType extends Enum {

    public static const BID:OrderActionType = new OrderActionType("BID");
    public static const OFFER:OrderActionType  = new OrderActionType("OFFER");
    public static const SELL:OrderActionType = new OrderActionType("SELL");
    public static const BUY:OrderActionType  = new OrderActionType("BUY");

    public function OrderActionType(name:String) {
        super(name, _);
    }

    override protected function getConstants():Array {
        return [BID, OFFER, SELL, BUY];
    }

    public function get oppositeOrderActionType():OrderActionType{
        var retVal : OrderActionType;
        switch(this)
        {
            case OrderActionType.BID:
                retVal = OrderActionType.OFFER;
                break;
            case OrderActionType.OFFER:
                retVal = OrderActionType.BID;
                break;
            case OrderActionType.BUY:
                retVal = OrderActionType.SELL;
                break;
            case OrderActionType.SELL:
                retVal = OrderActionType.BUY;
                break;
        }

        return retVal;
    }
//
//    /**
//     * Converts Fussion action type to Apollo(EBS) action type.
//     * Side define by InstrumentDO
//     * @return
//     */
//    public static function sideToActionType(sideType:int):OrderActionType{
//        var retVal:OrderActionType = OrderActionType.BID;
//        if(sideType == InstrumentDO.SIDE_SELL){
//            retVal = OrderActionType.OFFER;
//        }
//        return retVal;
//    }
//
//    /**
//     * Converts  Apollo(EBS) action type to Fusion side
//     * Side define by InstrumentDO
//     * @return
//     */
//    public static function actionToSideType(action:String):int{
//        action = action.toUpperCase();
//        var retVal:int = InstrumentDO.SIDE_SELL;
//        if(action == OrderActionType.BID.name || action == OrderActionType.BUY.name){
//            retVal =  InstrumentDO.SIDE_BUY;
//        }
//        return retVal;
//    }
}
}

