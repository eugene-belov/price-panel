package com.ebs.apollo.Constants{

public class AmountsUpdateType {
    public static const NEITHER:int	= 0;
    public static const PENDING:int	= 1;
    public static const DONE:int 	= 2;
    public static const BOTH:int    = 3;
}
}

