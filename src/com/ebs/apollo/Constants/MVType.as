package com.ebs.apollo.Constants
{
import com.ebs.apollo.utils.commons.Enum;

public class MVType extends Enum
	{
		public static const RATES 	         : MVType = new MVType(12, "RATES");
        public static const EBS_DEALS        : MVType = new MVType(15, "EBS_DEALS");
		public static const MARKET_DATA 	 : MVType = new MVType(20, "MARKET_DATA");
        public static const INDICATIVE_RATES : MVType = new MVType(24, "INDICATIVE_RATES");
        public static const WS_PRICES        : MVType = new MVType(34, "WS_PRICES");

        private var _type:uint;

        public function MVType(typeId:uint, name:String):void{
            super(name, _);
            _type = typeId;
        }

        public function get type():uint {
            return _type;
        }

        override protected function getConstants():Array {
            return [RATES, EBS_DEALS, MARKET_DATA, INDICATIVE_RATES, WS_PRICES];
        }
    }
}