package com.ebs.apollo.Constants{
import com.ebs.apollo.utils.commons.Enum;

public class DealPaidGivenType extends Enum {

    public static const GIVEN: DealPaidGivenType = new DealPaidGivenType(0, "Given");
    public static const PAID: DealPaidGivenType = new DealPaidGivenType(1, "Paid");

    private var _type:int;

    public function DealPaidGivenType(type:int, name:String):void{
        super(name, _);
        _type = type;
    }

    public function get type():int{
        return _type;
    }

    override protected function getConstants():Array {
        return [PAID, GIVEN];
    }
}
}

