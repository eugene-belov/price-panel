package com.ebs.apollo.Constants{
import com.ebs.apollo.utils.commons.Enum;

public class WorkBalanceType {
    public static const ALWAYS:int = 0;
    public static const PARTIAL:int = 1;
    public static const NEVER:int = 2;
}
}

