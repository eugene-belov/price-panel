package com.ebs.apollo.Constants{
import com.ebs.apollo.utils.commons.Enum;

public class TradeState extends Enum {

    public static const TRADE_STATUS_UNSUPPORTED: TradeState = new TradeState(0, "TradeStatusUnsupported");
    public static const PENDING: TradeState = new TradeState(1, "Pending");
    public static const ACCEPTED: TradeState = new TradeState(2, "Accepted");
    public static const QUERIED: TradeState = new TradeState(3, "Queried");
    public static const DONE: TradeState = new TradeState(4, "Done");
    public static const TIMED_OUT: TradeState = new TradeState(5, "TimedOut");
    public static const PENDING_TIME_OUT: TradeState = new TradeState(6, "PendingTimeout");

    private var _type:int;

    public function TradeState(type:int, name:String):void{
        super(name, _);
        _type = type;
    }

    public function get type():int{
        return _type;
    }

    override protected function getConstants():Array {
        return [TRADE_STATUS_UNSUPPORTED, PENDING, ACCEPTED, QUERIED, DONE, TIMED_OUT, PENDING_TIME_OUT];
    }
}
}

