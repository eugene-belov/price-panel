package com.ebs.apollo.Constants{
import com.ebs.apollo.utils.commons.Enum;

/**
 * Remote object available services
 */
public class RemoteServices extends Enum {

    public static const UPDATE_TRADER_PROFILE: RemoteServices = new RemoteServices("updateTraderProfile");
    public static const LOGIN: RemoteServices = new RemoteServices("login");
    public static const CONFIRM_HB_READY: RemoteServices = new RemoteServices("confirmHbConsumerReady");
    public static const SEND_HB: RemoteServices = new RemoteServices("sendHeartbeatFromClient");
    public static const ADD_MV_LISTENER: RemoteServices = new RemoteServices("subscribe2Mv");
    public static const REMOVE_MV_LISTENER: RemoteServices = new RemoteServices("unSubscribeFromMv");
    public static const REMOVE_ALL_MV_LISTENERS: RemoteServices = new RemoteServices("unsubscribe");
    public static const SUBMIT_ORDER: RemoteServices = new RemoteServices("order");
    public static const CANCEL_ORDER_BY_REF_ID: RemoteServices = new RemoteServices("cancelOrderByOrderReferenceId");
    public static const CANCEL_ORDER_BY_ORDER_ID: RemoteServices = new RemoteServices("cancelOrderByOrderId");
    public static const AMEND_ORDER: RemoteServices = new RemoteServices("amendOrderByOtherOrder");
    public static const AMEND_ORDER_REDUCE_AMOUNT: RemoteServices = new RemoteServices("amendOrderReduceAmount");
    public static const AMEND_ORDER_REDUCE_BY_TYPE: RemoteServices = new RemoteServices("amendOrderReduceAmountByType");

    public function RemoteServices(name:String):void{
        super(name, _);
    }

    override protected function getConstants():Array {
        return [UPDATE_TRADER_PROFILE, LOGIN, CONFIRM_HB_READY, SEND_HB, ADD_MV_LISTENER,
                REMOVE_MV_LISTENER, REMOVE_ALL_MV_LISTENERS, SUBMIT_ORDER, CANCEL_ORDER_BY_REF_ID,
                CANCEL_ORDER_BY_ORDER_ID, AMEND_ORDER, AMEND_ORDER_REDUCE_AMOUNT, AMEND_ORDER_REDUCE_BY_TYPE];
    }
}
}

