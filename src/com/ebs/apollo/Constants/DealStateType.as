package com.ebs.apollo.Constants{
import com.ebs.apollo.utils.commons.Enum;

public class DealStateType extends Enum {
    // Deal Status Values
    public static const TP_PENDING:                             MVType = new MVType(1);
    public static const TP_CONFIRMED:                           MVType = new MVType(2);
    public static const TP_PENDING_UNCONFIRMED_I:               MVType = new MVType(3);
    public static const TP_CONFIRMED_INSTRUCTIONS_TO_I:         MVType = new MVType(4);
    public static const TP_RECOVERED_DEAL_I:                    MVType = new MVType(5);
    public static const TP_RECOVERED_NO_INSTRUCTIONS_I:         MVType = new MVType(6);
    public static const TP_FAILED:                              MVType = new MVType(7);
    public static const TP_COMPLETED:                           MVType = new MVType(8);
    public static const TP_COMPLETED_NO_INSTRUCTIONS:           MVType = new MVType(9);
    public static const TP_COMPLETED_NO_INSTRUCTIONS_UNVERIFIED:MVType = new MVType(10);

    private var _type:uint;

    public function DealStateType(typeId:uint):void{
        super(String(typeId), _);
        _type = typeId;
    }

    public function get type():uint {
        return _type;
    }

    override protected function getConstants():Array {
        return [TP_PENDING, TP_CONFIRMED, TP_PENDING_UNCONFIRMED_I, TP_CONFIRMED_INSTRUCTIONS_TO_I,
            TP_RECOVERED_DEAL_I, TP_RECOVERED_NO_INSTRUCTIONS_I, TP_FAILED, TP_COMPLETED,
            TP_COMPLETED_NO_INSTRUCTIONS, TP_COMPLETED_NO_INSTRUCTIONS_UNVERIFIED];
    }
}
}

