package com.ebs.apollo.widgets.marketView {
import fusion.core.module.window.WindowContents;

import mx.events.FlexEvent;
import mx.messaging.Channel;
import mx.messaging.ChannelSet;

import mx.messaging.config.ServerConfig;

public class MarketViewWindow extends WindowContents {

    public function MarketViewWindow() {
        setStyle( "skinClass", Class(MarketViewWindowSkin));
    }

    override public function startUp():void {
        super.startUp();
        startUpComplete = true;
    }

}
}
