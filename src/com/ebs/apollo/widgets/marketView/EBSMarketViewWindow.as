package com.ebs.apollo.widgets.marketView {
import com.ebs.apollo.view.components.marketView.MarketViewToolbar;
import com.ebs.apollo.widgets.marketView.skins.EBSMarketViewWindowDarkSkin;

import flash.events.Event;

import fusion.core.host.visualComponents.skinnable.HSlider;

import fusion.core.host.visualComponents.skinnable.SkinnableList;

import fusion.core.module.window.WindowContents;

import mx.collections.ArrayCollection;

import mx.events.FlexEvent;
import mx.messaging.Channel;
import mx.messaging.ChannelSet;

import mx.messaging.config.ServerConfig;

public class EBSMarketViewWindow extends WindowContents {

    [Bindable]
    public var instruments:ArrayCollection = new ArrayCollection();
    [Bindable]
    public var numberOfColumns:int = 1;

    [SkinPart]
    public var instrumentList:SkinnableList;
    [SkinPart]
    public var marketViewToolbar:MarketViewToolbar;

    public function EBSMarketViewWindow() {

    }

    override protected function startUpCompleteCallbackHandler():void
    {
        super.startUpCompleteCallbackHandler();
        setSkin();
    }

    override public function startUp():void
    {
        super.startUp();
        startUpComplete = true;

        init();
    }

    override protected function partAdded(partName:String, instance:Object):void
    {
        super.partAdded(partName, instance);

        if( partName == "instrumentList" )
        {
            instrumentList.addEventListener(Event.SELECT, selectItemHandler);
        }
        else if( partName == "marketViewToolbar" )
        {
            marketViewToolbar.sizeSlider.addEventListener(Event.CHANGE, sizeChangeHandler);
            setSliderMinimum();
        }
    }

    private function setSkin():void
    {
        if(getStyle( "skinClass") == Class(EBSMarketViewWindowDarkSkin))
            return;

        setStyle( "skinClass", Class(EBSMarketViewWindowDarkSkin) );
    }

    public function setSliderMinimum():void
    {
        marketViewToolbar.sizeSlider.minimum = 74;
        marketViewToolbar.sizeSlider.maximum = 110;
//
//        marketViewToolbar.sizeSlider.value = (int) ((65 / 100.0) *
//                (marketViewToolbar.sizeSlider.maximum - marketViewToolbar.sizeSlider.minimum) + marketViewToolbar.sizeSlider.minimum);

        trace("setSliderMinimum", marketViewToolbar.sizeSlider.value);
    }

    private function init():void
    {
        for(var i:int = 0; i < 11; i++)
        {
            instruments.addItem({currency:"CNY 1M", date:"1/3-Apr"});
        }

    }

    private function selectItemHandler(event:Event):void
    {
        trace(event.target);
    }

    private function sizeChangeHandler(event:Event):void
    {
        // Store the percentage value, not the absolute pixel value
        var _delta:Number = (marketViewToolbar.sizeSlider.maximum - marketViewToolbar.sizeSlider.minimum),
            _percentage:Number = 100 * (marketViewToolbar.sizeSlider.value - marketViewToolbar.sizeSlider.minimum) / _delta;

        trace("sizeChangeHandler", event.currentTarget.value, _percentage)

//        if( _percentage < 65)
//        {
//            _percentage = 65;
//            marketViewToolbar.sizeSlider.value = (_percentage * _delta) / 100 + 69;
//        }
    }
}
}
