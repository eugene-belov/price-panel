package com.ebs.apollo.utils.commons {
public interface IEquals {
    /**
     * Indicates whether some other object is equal to this one.
     *
     * @param other the object to compare to this object for equality
     */
    function equals(other:Object):Boolean;
}
}
