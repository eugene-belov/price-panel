package com.ebs.apollo.utils.types {
import flash.utils.Dictionary;

import mx.collections.IList;

public function toArray(list:Object):Array {
    var ret:Array;
    if (list is IList)
        return IList(list).toArray();

    if (list instanceof Array)
        return (list as Array);

    if (list is Vector){
        ret = new Array(list.length);
        for (var idx:int = 0; idx < list.length; idx++){
            ret[idx] = list[idx];
        }
        return ret;
    }

    if(list is Dictionary){
        ret = new Array(list.length);
        for each (var item:Object in list) {
            ret.push(item);
        }
        return ret;
    }

    return [];
}
}
