package com.ebs.apollo.utils.types {
import flash.utils.getQualifiedClassName;

public function className(type:Class):String {
    var qualifiedClassName:String = getQualifiedClassName(type);
    return qualifiedClassName.substr(qualifiedClassName.indexOf("::") + 2);
}
}
