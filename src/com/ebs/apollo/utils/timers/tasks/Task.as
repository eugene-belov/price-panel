/**
 * Created with IntelliJ IDEA.
 * User: myakubov
 * Date: 22/01/14
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
package com.ebs.apollo.utils.timers.tasks {
import mx.utils.ObjectUtil;

public class Task {

    private var _type:TaskType;
    private var _terminateTime:Number;
    private var _isExecuting:Boolean = false;
    private var _instrumentId:String;

    private var _args:*;
    private var _handler:Function;

    public function Task(instrumentID:String) {
        _instrumentId = instrumentID;
    }

    public function execute():void{
        _isExecuting = true;
        trace("Task execute: " + ObjectUtil.toString(_type));
        _handler.apply(null, _args);
        _isExecuting = false;
    }

    public function get type():TaskType {
        return _type;
    }

    public function set type(value:TaskType):void {
        _type = value;
    }

    public function get handler():Function {
        return _handler;
    }

    public function set handler(value:Function):void {
        _handler = value;
    }

    public function get args():* {
        return _args;
    }

    public function set args(value:*):void {
        _args = value;
    }

    public function get terminateTime():Number {
        return _terminateTime;
    }

    public function set terminateTime(value:Number):void {
        _terminateTime = value;
    }

    public function get isExecuting():Boolean {
        return _isExecuting;
    }

    public function get instrumentId():String {
        return _instrumentId;
    }

    public function set instrumentId(value:String):void {
        _instrumentId = value;
    }
}
}
