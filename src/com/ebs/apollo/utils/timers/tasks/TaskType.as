/**
 * Created with IntelliJ IDEA.
 * User: myakubov
 * Date: 22/01/14
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
package com.ebs.apollo.utils.timers.tasks {
import fod.iConnect.commons.Enum;

public class TaskType extends Enum {
    public static const ORDER_HIT:TaskType = new TaskType("ORDER_HIT", 4000);
    public static const ORDER_TAKE:TaskType  = new TaskType("ORDER_TAKE", 4000);
    public static const ORDER_TERMINATE:TaskType = new TaskType("ORDER_TERMINATE", 5000);
    public static const NEW_PRICE_BID:TaskType  = new TaskType("NEW_PRICE_BID", 1000);
    public static const NEW_PRICE_OFFER:TaskType  = new TaskType("NEW_PRICE_OFFER", 1000);

    private var _latency:int = 0;

    public function TaskType(name:String, latencyMilis:int) {
        _latency = latencyMilis;
        super(name, _);
    }

    override protected function getConstants():Array {
        return [ORDER_HIT, ORDER_TAKE, ORDER_TERMINATE, NEW_PRICE_BID, NEW_PRICE_OFFER];
    }

    public function get latency():int {
        return _latency;
    }
}
}
