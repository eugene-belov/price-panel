/**
 * Created with IntelliJ IDEA.
 * User: myakubov
 * Date: 19/01/14
 * Time: 14:13
 * To change this template use File | Settings | File Templates.
 */
package com.ebs.apollo.utils.timers.tasks {
import flash.utils.Timer;

public class DataTimer extends Timer {
    private var _data:Object;

    public function DataTimer(delay:Number, repeatCount:int = 0) {
        super(delay, repeatCount);
        _data = new Object();
    }

    public function get data():Object {
        return _data;
    }

    public function set data(value:Object):void {
        _data = value;
    }
}
}
