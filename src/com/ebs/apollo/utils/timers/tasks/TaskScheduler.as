/**
 * Created with IntelliJ IDEA.
 * User: myakubov
 * Date: 22/01/14
 * Time: 13:45
 * To change this template use File | Settings | File Templates.
 */
package com.ebs.apollo.utils.timers.tasks {

import flash.events.TimerEvent;
import flash.utils.Timer;

import fod.Apollo.utils.functional._;
import fod.Apollo.utils.functional.filter;
import fod.Apollo.utils.functional.find;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.utils.ObjectUtil;
import mx.utils.ObjectUtil;

public class TaskScheduler {

    public static const MIN_LATENCY:int = 300;
    public static const MAX_LATENCY:int = 300000;

    private static var _instance:TaskScheduler;

    [ArrayElementType("fod.Apollo.utils.timers.tasks.Task")]
    private var _tasks:ArrayCollection = new ArrayCollection();
    private var _tasksTimer:Timer;

    public function TaskScheduler(initializer:SingletonInitializer) {
    }

    public static function get Instance():TaskScheduler {
        if (_instance == null)
            _instance = new TaskScheduler(new SingletonInitializer());
        return _instance;
    }

    public function addTask(newTask:Task):void {
        if (newTask) {
            newTask.terminateTime = new Date().getTime() + newTask.type.latency;
            _tasks.addItem(newTask);
            tuneTasksTimer();
        }
    }

    public function removeTask(task:Task):void {
        if (task) {
            var currTask:Task;
            for (var idx:int = 0; idx < _tasks.length; idx++) {
                currTask = _tasks[idx];
                if (currTask.instrumentId == task.instrumentId &&
                        currTask.type.equals(task.type) &&
                        currTask.isExecuting == false) {
                    _tasks.removeItemAt(idx);
                    break;
                }
            }
            if (_tasks.length == 0) {
                tasksTimer.stop();
            }
        }
    }

    public function isTaskPending(task:Task):Boolean {
        return (findTask(task) != null);
    }

    private function findTask(task:Task):Task {
        var filteredTasks = filter(_tasks, _..instrumentId.equals(task.instrumentId));
        var foundTask:Task = find(filteredTasks, _..type.name.equals(task.type.name));
        return foundTask;
    }

    private function tuneTasksTimer():void {
        //Trigger on next task termination...
        var nextTrigger:Number = getNextTerminateTime();
        if (nextTrigger > MAX_LATENCY) {
            //TODO<Michael>: log??
            Alert.show("tuneTasksTimer: ERROR! " + ObjectUtil.toString(_tasks));
        } else {
            tasksTimer.stop();
            tasksTimer.delay = nextTrigger;
            tasksTimer.start();
        }
    }

    private function getNextTerminateTime():Number {
        var delta:Number;
        var minDelta:Number = Number.MAX_VALUE;
        var currTime:Number = new Date().getTime();
        for each (var currTask:Task in _tasks) {
            delta = currTask.terminateTime - currTime;
            if (delta < minDelta) {
                minDelta = delta;
            }
        }
        minDelta = Math.abs(minDelta);
        return minDelta;
    }

    private function executeTasksHandler():void {
        var delta:Number;
        var currTime:Number = new Date().getTime();
        var removeTasksList:Array = new Array();
        for each (var currTask:Task in _tasks) {
            delta = currTask.terminateTime - currTime;
            //Execute all tasks designated for the near MIN_LATENCY
            if (delta <= MIN_LATENCY) {
                currTask.execute();
                removeTasksList.push(currTask);
            }
        }

        removeTasksList.forEach(function (task:Task, index:Number, arr:Array) {
            removeTask(task)
        });

        if (_tasks.length > 0) {
            tuneTasksTimer();
        }
    }

    private function get tasksTimer():Timer {
        if (!_tasksTimer) {
            _tasksTimer = new Timer(5000, 1);
            _tasksTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timerComplete);
        }
        return _tasksTimer;
    }

    private function timerComplete(event:TimerEvent):void {
        executeTasksHandler();
    }
}
}

/** Used for ensuring a class is a singleton **/
class SingletonInitializer {
}